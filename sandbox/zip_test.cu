#include <thrust/iterator/zip_iterator.h>
#include <thrust/device_vector.h>
#include <thrust/tuple.h>
#include <thrust/reduce.h>
#include <thrust/functional.h>

template <class T>
struct even_max {
  __host__ __device__ 
   T operator() (T a, T b) {
    char cha = thrust::get<1>(a);
    char chb = thrust::get<1>(b);
    if (cha =='x' && chb !='x') return a;
    if (cha !='x' && chb =='x') return b;
    return thrust::maximum<T>()(a,b);
  }
};
int main(void) {

thrust::device_vector<int> A(4);
thrust::device_vector<char> B(4);

A[0] = 4;
A[1] = 10;
A[2] = 8;
A[3] = 3;
B[0] = 'x';
B[1] = 'b';
B[2] = 'x';
B[3] = 'g';


auto first = thrust::make_zip_iterator(thrust::make_tuple(A.begin(), B.begin()));
auto last = thrust::make_zip_iterator(thrust::make_tuple(A.end(), B.end()));

std::cout << (*++first).get<0>() << std::endl;
//std::cout << (*first++).get<0>() << std::endl;
//std::cout << (*first++).get<0>() << std::endl;

thrust::maximum< thrust::tuple<int, char> > binary_op;
thrust::tuple<int, char> init = first[0];
auto result = thrust::reduce(first, last, init, even_max<thrust::tuple<int,char>>());
std::cout << thrust::get<0>(result) << thrust::get<1>(result) << std::endl;
return 0;
}
