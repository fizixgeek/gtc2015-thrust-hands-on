#include <iostream>

#include "thrust/device_vector.h"

int main(void) {

   thrust::device_vector<char> scramble(7);
   thrust::device_vector<int> labels(7);
   
   scramble[0] = 'u';
   scramble[1] = 'h';
   scramble[2] = 's';
   scramble[3] = 'r';
   scramble[4] = 't';
   scramble[5] = 't';
   labels[0] = 5;
   labels[1] = 1;
   labels[2] = 3;
   labels[3] = 0;
   labels[4] = 2;
   labels[5] = 4;

   auto start = thrust::make_permutation_iterator(scramble.begin(), labels.begin());
   auto finish = thrust::make_permutation_iterator(scramble.begin(), labels.end());

   for(auto i  =  start; i != finish; i++) std::cout << *i << std::endl;
}

