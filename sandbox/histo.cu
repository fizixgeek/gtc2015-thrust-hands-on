#include <iostream>
#include <thrust/device_vector.h>
#include <thrust/iterator/constant_iterator.h>
#include <thrust/sort.h>


using namespace thrust::placeholders;

int main(void) {

   //generate random integers to 100
   thrust::host_vector<int> h_data(1000);
   thrust::generate(h_data.begin(), h_data.end(), rand);
   thrust::device_vector<int> data =h_data;
   thrust::transform(data.begin(), data.end(), data.begin(), _1%100);
   
   thrust::sort(data.begin(), data.end());

   thrust::device_vector<int> groups(100);
   thrust::device_vector<int> freq(100);
   thrust::reduce_by_key(data.begin(), data.end(), 
                         thrust::make_constant_iterator<int>(1),
                         groups.begin(), freq.begin(),
                         thrust::equal_to<int>(), thrust::plus<int>());
   auto j = freq.begin();
   for (auto i = groups.begin();i != groups.end(); i++, j++) 
   {
      std::cout << *i << ": " << *j << std::endl;
   }
  return 0;
}
