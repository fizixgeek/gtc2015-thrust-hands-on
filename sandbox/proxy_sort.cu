#include <iostream>

#include "thrust/device_vector.h"
#include "thrust/sort.h"

int main(void) {

   thrust::device_vector<char> scramble(7);
   thrust::device_vector<int> labels(7);
   
   scramble[5] = 'l'; labels[5] = 0;
   scramble[4] = 'i'; labels[4] = 1;
   scramble[0] = 'b'; labels[0] = 2;
   scramble[6] = 'e'; labels[6] = 3;
   scramble[2] = 'r'; labels[2] = 4;
   scramble[1] = 't'; labels[1] = 5;
   scramble[3] = 'y'; labels[3] = 6;

   thrust::device_vector<int> map(7);
   thrust::copy(thrust::make_counting_iterator<int> (0),
                thrust::make_counting_iterator<int> (7),
                map.begin());
   std::cout << "map (unsorted): ";
   for(auto i  =  map.begin(); i != map.end(); i++) std::cout << *i ;
   std::cout << std::endl;
   
   thrust::sort_by_key(labels.begin(), labels.end(), map.begin());
   
   std::cout << "map (sorted by key): ";
   for(auto i  =  map.begin(); i != map.end(); i++) std::cout << *i ;
   std::cout << std::endl;

   auto start = thrust::make_permutation_iterator(scramble.begin(), map.begin());
   auto finish = thrust::make_permutation_iterator(scramble.begin(), map.end());

   for(auto i  =  start; i != finish; i++) std::cout << *i ;
   std::cout << std::endl;
   return 0;
}

