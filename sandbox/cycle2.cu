#include <iostream>

#include "thrust/device_vector.h"
#include "thrust/iterator/counting_iterator.h"

using namespace thrust;
struct loop : public unary_function<int,int> {
   __host__ __device__ int operator() (int i) {
      return i%26;
   } 
};
int main(void) {

   char* alpha = "abcdefghijklmnopqrstuvwxyz";
   host_vector<char> h_A(26);
   std::copy(alpha,alpha+26,h_A.begin());
   device_ptr<char> pA;
   thrust::copy(h_A.begin(), h_A.end(), pA);
   device_vector<char> A(pA,pA+26);
   for (int q=0;q<25;q++) {
      A[q] = 'a'+q;
   }

   std::cout << A[3] << A[5] << std::endl;

   auto perm_first = make_transform_iterator(counting_iterator<int>(0), loop());
   //auto perm_first = make_transform_iterator(counting_iterator<int>(), thrust::negate<int>());

   auto first = make_permutation_iterator(A.begin(), perm_first);

   std::cout << *first << std::endl;
   std::cout << *++first << std::endl;
   std::cout << *++first << std::endl;
   std::cout << *++first << std::endl;
   std::cout << *++first << std::endl;
   std::cout << *++first << std::endl;
   std::cout << *++first << std::endl;
   first += 23;
   std::cout << *++first << std::endl;
   std::cout << *++first << std::endl;
   std::cout << *++first << std::endl;
}
