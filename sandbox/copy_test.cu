#include <iostream>
#include "thrust/copy.h"
#include "thrust/device_vector.h"
#include "thrust/sequence.h"
#include "thrust/count.h"

struct isodd {
   __host__ __device__ bool operator()(int in) {return in%2==1;}
};
bool gt5 (int in) {return in>5;}
using namespace std::placeholders;
#if 1
int main(void) {

   thrust::device_vector<int> A(100);
   thrust::device_vector<int> B(100);
   thrust::sequence(A.begin(), A.end());

//   thrust::device_ptr<int> B = thrust::copy_if(A.begin(), A.end(),
 //    [] (int i) {return i%2==1;});
   auto B_end = thrust::copy_if(A.begin(), A.end(), B.begin(),
     isodd());
   //auto B_end = thrust::copy_if(A.begin(), A.end(), B.begin(), gt5);
   //auto B_end = thrust::copy_if(A.begin(), A.end(), B.begin(), [] (int in) {return in>5;});
   //auto B_end = thrust::copy_if(A.begin(), A.end(), B.begin(), _1>5);
   

   std::cout << B[4] << std::endl;
   std::cout << B[5] << std::endl;

   std::cout << thrust::count_if(A.begin(), A.end(), isodd()) <<std::endl;

   return 0;
}
#else

using namespace thrust;
int main(void) {
   thrust::device_vector<int> output(5);

   float stencil[5];
   stencil[0] = 0;
   stencil[1] = 0;
   stencil[2] = 1;
   stencil[3] = 0;
   stencil[4] = 1;
   device_ptr<float> tp_stencil = device_pointer_cast(stencil);

   device_ptr<int> output_end = copy_if(make_counting_iterator<int>(0), 
     make_counting_iterator<int>(5), 
     tp_stencil, 
     output.begin(), 
     _1 == 1);

   int number_of_ones = output_end - output.begin();

}
#endif

