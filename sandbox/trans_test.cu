#include <iostream>
#include "thrust/device_vector.h"
#include "thrust/transform.h"
#include "thrust/generate.h"
#include "thrust/sequence.h"

struct add_two : public thrust::unary_function<int,int> {
__host__ __device__ int operator()(int in) { 
   return in+2;   
}
};
struct add_three {
   __host__ __device__ int operator()(int in) { return in+3;}
};
__host__ __device__ int add_four(const int& in) { return in+4;} 
int main(void) {
  
  thrust::device_vector<int> A(100);
  thrust::device_vector<int> B(100);
  thrust::sequence(A.begin(), A.end());

  //thrust::transform(A.begin(), A.end(), B.begin(), thrust::negate<int>());
  //thrust::transform(A.begin(), A.end(), B.begin(), [] (int in) {return in+2;});
  //thrust::transform(A.begin(), A.end(), B.begin(), add_two());
  //thrust::transform(A.begin(), A.end(), B.begin(), add_three());
  thrust::transform(A.begin(), A.end(), B.begin(), add_four);

  std::cout << B[4] << std::endl;
  std::cout << B[5] << std::endl;


  return 0;
}
