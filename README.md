Slides, code samples and video from the GTC 2015 Thrust hands-on lab. Clone the repo to a location Read through the slides and/or watch the video, taking time to work through the tasks specified.

The goal of the lab is to accelerate kmeans using thrust and cuBLAS. We begin with a working CPU-only implementation and, applying a number of thrust tools, move the computation to the GPU. 

The exercises begin very gently and get more challenging as they go. 

Prerequisites:
CUDA 7.0
Kepler GPU
GCC 4.8.0 or other CPU compiler supporting C++11

If you get confused or have questions, don't forget the thrust-users google group. https://groups.google.com/forum/#!forum/thrust-users