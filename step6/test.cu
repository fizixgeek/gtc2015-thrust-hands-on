#include <iostream>
#include <vector>
#include "stdlib.h"
#include "string.h" //memset
#include <cfloat> //DBL_MAX
//#include "mkl.h"
#include "cublas_v2.h"
#include "thrust/host_vector.h"
#include "thrust/generate.h"
#include "thrust/device_vector.h"
#include "thrust/inner_product.h"
#include "thrust/sort.h"
#include "thrust/iterator/counting_iterator.h"
#include "thrust/iterator/permutation_iterator.h"

using namespace thrust::placeholders;
cublasHandle_t h;
void random_data(thrust::host_vector<double>& array, int n) {
//Fill an array with random double 0 to 1
   for (int i=0; i<n; i++) array[i] = (double)rand()/(double)RAND_MAX;
}

void random_labels(thrust::host_vector<int>& array, int n, int max) {
//Fill an array with random integers 0 to max
   for (int i=0; i<n; i++) array[i] = rand()%max;
}

void self_dot(thrust::host_vector<double> array_in, int n, int dim, 
              thrust::host_vector<double>& dots, bool transpose=false) {
//compute the inner product of each row with itself (i.e. the sum
//  of the squares of al lthe elements in each row)
//If transpose is true, transpose the matrix
   if (transpose) {
      for (int pt = 0; pt<n; pt++) {
         double sum = 0.0;
         for (int i=0; i<dim; i++) {
            sum += array_in[pt+i*n]*array_in[pt+i*n];
         }
         dots[pt] = sum;
      }
   } else {
      for (int pt = 0; pt<n; pt++) {
         dots[pt] = thrust::inner_product(array_in.begin()+pt*dim, 
                                          array_in.begin()+(pt+1)*dim, 
                                          array_in.begin()+pt*dim, 0.0);
      }
   }
}

void find_centroids(thrust::device_vector<double> d_array, int n, int dim, 
                    thrust::host_vector<int> labels_in,
                    thrust::host_vector<double>& centroids, int n_cluster) {
//Find new centroids by averaging all the points in the cluster

   thrust::host_vector<int> members(n_cluster); //Number of points in each cluster
   memset(&members[0], 0, n_cluster*sizeof(int));
   thrust::device_vector<double> d_centroids(n_cluster*dim);
   thrust::device_vector<int> d_labels = labels_in;
   thrust::device_vector<int> d_dims_out(n_cluster);
   thrust::device_vector<int> d_members(n_cluster);

   //Add all vectors in the cluster
   thrust::device_vector<int> map(n);
   thrust::copy(thrust::make_counting_iterator<int>(0),
                thrust::make_counting_iterator<int>(n), map.begin());
   thrust::sort_by_key(d_labels.begin(), d_labels.end(), map.begin());
   for (int i=0;i<dim;i++) {
      auto sorted_row = 
            thrust::make_permutation_iterator(d_array.begin()+n*i, map.begin());
      thrust::reduce_by_key(d_labels.begin(), d_labels.end(),
                            sorted_row, d_dims_out.begin(),
                            d_centroids.begin()+i*n_cluster,
                            thrust::equal_to<int>(),
                            thrust::plus<double>());
   }
   //Count the points in each cluster
   thrust::device_vector<int> ones(d_labels.size(),1);
   thrust::reduce_by_key(d_labels.begin(), d_labels.end(), ones.begin(),
                         d_dims_out.begin(), d_members.begin(),
                         thrust::equal_to<int>(), thrust::plus<int>());

   for(int i=0; i<dim;i++) {
      thrust::transform(d_centroids.begin()+i*n_cluster,
                        d_centroids.begin()+(i+1)*n_cluster,
                        d_members.begin(),
                        d_centroids.begin()+i*n_cluster,
                        _1/_2);
   }
   centroids = d_centroids;
}

void compute_distances(thrust::device_vector<double> d_data, 
                       int n, int dim, thrust::host_vector<double> centroids_in, 
                       thrust::host_vector<double> centroid_dots, int n_cluster, 
                       thrust::host_vector<double>& pairwise_distances) {
//Compute the distance from each data point to each centroid via the
// formula
//     (p-c).(p-c) = p.p + c.c - 2 p.c

   //First build p.p+c.c
   self_dot(centroids_in, n_cluster, dim, centroid_dots, true);
   for (int nn=0; nn<n; nn++) 
      for (int cc=0; cc<n_cluster; cc++) {
         pairwise_distances[nn*n_cluster+cc] = centroid_dots[cc];
      }
   //Now use BLAS to add in -2p.c
   double alpha = -2.0;
   double beta = 1.0;
   thrust::device_vector<double> d_centroids = centroids_in;
   thrust::device_vector<double> d_pairwise_distances = pairwise_distances;

   double* p_data = thrust::raw_pointer_cast(d_data.data());
   double* p_centroids = thrust::raw_pointer_cast(d_centroids.data());
   double* p_pairwise_distances = thrust::raw_pointer_cast(d_pairwise_distances.data());
   cublasDgemm(h, CUBLAS_OP_N, CUBLAS_OP_T, n_cluster, n, dim,
               &alpha, p_centroids, n_cluster, p_data, n, 
               &beta, p_pairwise_distances, n_cluster);
   //FIXME: Once relabel is working on the GPU, don't copy this data to the CPU
   pairwise_distances = d_pairwise_distances;
}
int relabel(int n, thrust::host_vector<double> pairwise_distances_in,
             int n_cluster, thrust::host_vector<int>& labels) {
//Relabel each point by finding it's nearest centroid

   //Find the index of the minimum value in each row of pairwise_distances
   //FIXME: Use your thrust tools to do this on the GPU
   int changes = 0; 
   for (int nn=0; nn<n; nn++) {
      double min = DBL_MAX;
      int idx = -1;
      for (int cc=0; cc<n_cluster; cc++) {
         double this_dist = pairwise_distances_in[nn*n_cluster+cc];
         if (this_dist < min) {
            idx=cc;
            min=this_dist;
         }
      }
      if (labels[nn] != idx) {
         changes++; 
         labels[nn] = idx;
      }
   }
   return changes;
}
int main(int argc, char** argv) {

   int n = 5e5; //the number of input data points
   int d = 50; //the dimension of the space 
               // (i.e. the number of values for each data point
   int n_cluster = 100; //the number of clusters to create
   int iterations = 10; 

   if (argc>1) {
     if (0==strcmp(argv[1], "--help")) {
        std::cout << "Usage: test [<number of points> [<dimension of space>"
                     " [<number of clusters> [<iterations>]]]]" << std::endl;
        return 0;
     }
     else n = atoi(argv[1]);
   }
   if (argc>2) d = atoi(argv[2]);
   if (argc>3) n_cluster = atoi(argv[3]);
   if (argc>4) iterations = atoi(argv[4]);

   std::cout << "Generating random data" << std::endl;
   std::cout << n << " points of dimension " << d << std::endl;
   std::cout << n_cluster << " clusters" << std::endl;
   
   thrust::host_vector<double> data(n*d); //input data (n rows of d)
   thrust::host_vector<double> centroids(n_cluster*d); //centroids for each cluster
                                               //(n_cluster rows of d)
   thrust::host_vector<int> labels(n); //cluster labels for each point
   thrust::host_vector<double> distances(n); //distances from point from a centroid

   thrust::generate(data.begin(), data.end(), 
                     [](){return (double)rand()/(double)RAND_MAX;});
 

   thrust::host_vector<double> centroid_dots(n_cluster); //self dot product of each
                                                 //centroid
   thrust::host_vector<double> pairwise_distances(n_cluster * n); //distance from each
                                                          //point to each
                                                          //centroid
                                                          //n rows of n_cluster
   thrust::host_vector<int> labels_copy(n);

   thrust::device_vector<double> d_data = data;

   //Let the first n_cluster points be the centroids of the clusters
   for(int nn=0;nn<n_cluster;nn++)
      for (int dd=0;dd<d;dd++)
         centroids[nn+n_cluster*dd] = data[nn+n*dd];
   
   cublasCreate(&h);
   for(int i=0; i<iterations; i++) {
      compute_distances(d_data, n, d, centroids, centroid_dots, 
                        n_cluster, pairwise_distances);
      int movers = relabel(n, pairwise_distances, n_cluster, labels);
      std::cout <<std::endl << "*** Iteration " << i+1 << " ***" << std::endl;
      std::cout << movers << " points moved between clusters." << std::endl;
      if (0 == movers) break;
      find_centroids(d_data, n, d, labels, centroids, n_cluster);
   }
   cublasDestroy(h);
}
